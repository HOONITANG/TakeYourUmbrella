package com.kemiz.takeyourumbrella.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kemiz.takeyourumbrella.Data.TimeHorizontalViewSectionHeader
import com.kemiz.takeyourumbrella.Util.DateConvert
import java.util.Date

class WeatherTimeSectionViewModel: ViewModel() {
    val data = MutableLiveData<List<TimeHorizontalViewSectionHeader>>()
    private var firstTomorrow = true
    private var secondTomorrow = true

    fun updateData(newData: List<Date>?) {
        val value = newData?.map { TimeHorizontalViewSectionHeader(title = convertValue(it, newData))}
        data.value = value ?: listOf()
    }

    private fun convertValue(date: Date?, newData: List<Date>?): String  {
        val dateConverter = DateConvert()
        var time = dateConverter.extractFormattedDateTimeFromDate(date ?: Date())
        var hour = dateConverter.convertKstToHour(time)

        if (firstTomorrow && dateConverter.isNextDay(time, 1)) {
            firstTomorrow = false
            hour = "내일"
        }
        else if (secondTomorrow && dateConverter.isNextDay(time, 2)) {
            secondTomorrow = false
            hour = "모레"
        }
        else {
            hour = "${hour}시"
        }

        return hour
    }
}