package com.kemiz.takeyourumbrella.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kemiz.takeyourumbrella.Data.IntegratedWeatherModel

class OpenWeatherTimeViewModel: ViewModel() {
    val data = MutableLiveData<List<IntegratedWeatherModel>>()

    fun updateData(newData: List<IntegratedWeatherModel>?) {
        data.value = newData ?: listOf()
    }
}