package com.kemiz.takeyourumbrella.Screen


import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.location.LocationServices
import com.kemiz.takeyourumbrella.Adapter.WeatherTimeAdapter
import com.kemiz.takeyourumbrella.Data.APIMethod
import com.kemiz.takeyourumbrella.Data.IntegratedWeatherModel
import com.kemiz.takeyourumbrella.Data.TimeHorizontalViewSectionHeader
import com.kemiz.takeyourumbrella.Data.VerticalItem
import com.kemiz.takeyourumbrella.R
import com.kemiz.takeyourumbrella.databinding.ActivityMainBinding
import com.kemiz.takeyourumbrella.databinding.FragmentHomeBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.Locale
import com.kemiz.takeyourumbrella.Adapter.WeatherTimeSectionAdapter
import com.kemiz.takeyourumbrella.Util.LocationCallbackInterface
import com.kemiz.takeyourumbrella.Util.LocationHelper
import com.kemiz.takeyourumbrella.ViewModel.GisangWeatherTimeViewModel
import com.kemiz.takeyourumbrella.ViewModel.OpenWeatherTimeViewModel
import com.kemiz.takeyourumbrella.ViewModel.TomorrowWeatherTimeViewModel
import com.kemiz.takeyourumbrella.ViewModel.WeatherTimeSectionViewModel

class HomeFragment : Fragment(), LocationCallbackInterface {

    private lateinit var sharedWeatherViewModel: WeatherTimeSectionViewModel
    private lateinit var sharedGisangViewModel: GisangWeatherTimeViewModel
    private lateinit var sharedTomorrowViewModel: TomorrowWeatherTimeViewModel
    private lateinit var sharedOpenViewModel: OpenWeatherTimeViewModel

    private lateinit var weatherTimeSectionAdapter: WeatherTimeSectionAdapter
    private lateinit var gisangWeatherTimeAdapter: WeatherTimeAdapter
    private lateinit var tomorrowWeatherTimeAdapter: WeatherTimeAdapter
    private lateinit var openWeatherTimeAdapter: WeatherTimeAdapter
    private lateinit var locationHelper: LocationHelper

    private lateinit var locationTextView: TextView
    private lateinit var lastUpdatedTimeTextView:TextView

    lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentHomeBinding.inflate(layoutInflater)
        /* 데이터 옵저버 연결 */
        bind()
        lastUpdatedTimeTextView = binding.lastUpdatedTime
        /* 뷰 새로고침 */
        val swipeRefreshLayout: SwipeRefreshLayout = binding.swipeRefreshLayout

        /* 위치 설정하기 */
        locationTextView = binding.location
        // getLocation(locationTextView)

        // 위치 정보 권한 요청 및 위치 정보

        locationHelper = LocationHelper(requireContext(), this)
        locationHelper.requestLocationUpdates()

        //val location = RequestPermissionsUtil(requireContext()).getLocation()
        //val address = getAddress(location.latitude, location.longitude)

//        address?.takeIf { it.isNotEmpty() }?.get(0)?.let { firstAddress ->
//            locationTextView.text = "${firstAddress.adminArea} ${firstAddress.locality} ${firstAddress.thoroughfare}"
//        }
//        Log.d("kkim","겟 로케이션!!!!!! ${location.latitude} ${location.longitude}")

        swipeRefreshLayout.setOnRefreshListener {
            GlobalScope.launch(Dispatchers.Main) {
                try {
                    getLocation(locationTextView)
                } catch (e: Exception) {
                    showToast("Error: ${e.message}")
                } finally {
                    swipeRefreshLayout.isRefreshing = false
                }
            }
        }

        /*
        // 어댑터 데이터 예시
        val dataList: List<IntegratedWeatherModel> = listOf(
            IntegratedWeatherModel(Date(), "1","1",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "2","2",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "3","3",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "1","1",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "2","2",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "3","3",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "1","1",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "2","2",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "3","3",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "1","1",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "2","2",APIMethod.GISANG),
            IntegratedWeatherModel(Date(), "3","3",APIMethod.GISANG)
        )
        */
        /* 날씨 리스트 화면 데이터 설정*/
        /* 1) 시간 리스트뷰 설정*/
        var weatherTimeSectionRecyclerView: RecyclerView = binding.weatherTimeSectionRecyclerView
        weatherTimeSectionRecyclerView.isNestedScrollingEnabled = false;
        weatherTimeSectionRecyclerView.layoutManager = LinearLayoutManager(context,
            LinearLayoutManager.HORIZONTAL,
            false)
        weatherTimeSectionAdapter = WeatherTimeSectionAdapter(listOf())
        weatherTimeSectionRecyclerView.adapter = weatherTimeSectionAdapter

        /* 1) 기상청 리스트뷰 설정*/
        var gisangWeatherTimeRecyclerView: RecyclerView = binding.gisangWeatherTimeRecyclerView
        gisangWeatherTimeRecyclerView.isNestedScrollingEnabled = false;
        gisangWeatherTimeRecyclerView.layoutManager = LinearLayoutManager(context,
            LinearLayoutManager.HORIZONTAL,
            false)
        gisangWeatherTimeAdapter = WeatherTimeAdapter(listOf())
        gisangWeatherTimeRecyclerView.adapter = gisangWeatherTimeAdapter

        /* 2) 투모로우 리스트뷰 설정*/
        var tomorrowWeatherTimeRecyclerView: RecyclerView = binding.tomorrowWeatherTimeRecyclerView
        tomorrowWeatherTimeRecyclerView.isNestedScrollingEnabled = false;
        tomorrowWeatherTimeRecyclerView.layoutManager = LinearLayoutManager(context,
            LinearLayoutManager.HORIZONTAL,
            false)
        tomorrowWeatherTimeAdapter = WeatherTimeAdapter(listOf())
        tomorrowWeatherTimeRecyclerView.adapter = tomorrowWeatherTimeAdapter

        /* 3) 오픈웨더 리스트뷰 설정*/
        var openWeatherTimeRecyclerView: RecyclerView = binding.openWeatherTimeRecyclerView
        openWeatherTimeRecyclerView.isNestedScrollingEnabled = false;
        openWeatherTimeRecyclerView.layoutManager = LinearLayoutManager(context,
            LinearLayoutManager.HORIZONTAL,
            false)
        openWeatherTimeAdapter = WeatherTimeAdapter(listOf())
        openWeatherTimeRecyclerView.adapter = openWeatherTimeAdapter

        // 간격 설정
        // val itemDecoration = HorizontalSpaceItemDecoration(resources.getDimensionPixelSize(R.dimen.item_space))
        // recyclerView.addItemDecoration(itemDecoration)

        /* 고정 오픈 날씨 제공 API 뷰설정 */
        val timeItemIds = listOf(com.kemiz.takeyourumbrella.R.id.timeItemInclude1, com.kemiz.takeyourumbrella.R.id.timeItemInclude2, com.kemiz.takeyourumbrella.R.id.timeItemInclude3)
        val degreesTexts = listOf("기상청", "tomorrow.io", "openWeather")
        val imageResources = listOf(com.kemiz.takeyourumbrella.R.drawable.gisang, com.kemiz.takeyourumbrella.R.drawable.tomorrow, com.kemiz.takeyourumbrella.R.drawable.openweather)

        for (i in timeItemIds.indices) {
            val timeItem: View = binding.root.findViewById(timeItemIds[i])
            setWeatherApiViewInfo(timeItem, degreesTexts[i], imageResources[i])
        }

        return binding.root
    }

    fun setWeatherApiViewInfo(timeItem: View, degreesText: String, imageResource: Int) {
        timeItem.findViewById<TextView>(com.kemiz.takeyourumbrella.R.id.timeDegreesTextView).text = degreesText
        timeItem.findViewById<ImageView>(com.kemiz.takeyourumbrella.R.id.imageViewTimeVertical).setImageResource(imageResource)
    }

    // Method to update the text of the TextView
    fun updateLastedTimeText(newText: String) {
        lastUpdatedTimeTextView.text = newText
    }

    @SuppressLint("MissingPermission")
    private fun getLocation(textView: TextView) {
        val context = requireContext()
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(context)

        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener { success: Location? ->
                success?.let { location ->
                    val address = getAddress(location.latitude, location.longitude)?.get(0)
                    Log.d("kkim","겟 로케이션!!!!!! ${location.latitude} ${location.longitude}")
                    textView.text =
                        address?.let {
                            "${it.adminArea} ${it.locality} ${it.thoroughfare}"
                        }
                }
            }
            .addOnFailureListener { fail ->
                textView.text = "위치 권한이 필요합니다."
            }
    }

    private fun getAddress(lat: Double, lng: Double): List<Address>? {
        lateinit var address: List<Address>
        val context = requireContext()
        return try {
            val geocoder = Geocoder(context, Locale.KOREA)
            address = geocoder.getFromLocation(lat, lng, 1) as List<Address>
            address
        } catch (e: IOException) {
            showToast("주소를 가져 올 수 없습니다")
            null
        }
    }

    private fun showToast(message: String) {
        val context = requireContext()
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun bind() {
        // viewModel

        /* 시간 타이틀 데이터 갱신 */
        sharedWeatherViewModel = ViewModelProvider(requireActivity()).get(WeatherTimeSectionViewModel::class.java)
        // ViewModel의 데이터 변경을 감지하고 RecyclerView 갱신
        sharedWeatherViewModel.data.observe(viewLifecycleOwner) { newData ->
            Log.d("kkim", "데이터가 갱신됐따! ${newData}")
            weatherTimeSectionAdapter.updateData(newData)
        }

        /* 기상청 데이터 갱신 */
        sharedGisangViewModel = ViewModelProvider(requireActivity()).get(GisangWeatherTimeViewModel::class.java)
        // ViewModel의 데이터 변경을 감지하고 RecyclerView 갱신
        sharedGisangViewModel.data.observe(viewLifecycleOwner) { newData ->
            Log.d("kkim", "데이터가 갱신됐따! ${newData}")
            // adapter.submitList(newData)
            // gisangWeatherTimeAdapter.up
            gisangWeatherTimeAdapter.updateData(newData)
        }

        /* 투모로우 데이터 갱신 */
        sharedTomorrowViewModel = ViewModelProvider(requireActivity()).get(TomorrowWeatherTimeViewModel::class.java)
        // ViewModel의 데이터 변경을 감지하고 RecyclerView 갱신
        sharedTomorrowViewModel.data.observe(viewLifecycleOwner) { newData ->
            Log.d("kkim", "데이터가 갱신됐따! ${newData}")
            // adapter.submitList(newData)
            tomorrowWeatherTimeAdapter.updateData(newData)
        }

        /* 오픈웨더 데이터 갱신 */
        sharedOpenViewModel = ViewModelProvider(requireActivity()).get(OpenWeatherTimeViewModel::class.java)
        // ViewModel의 데이터 변경을 감지하고 RecyclerView 갱신
        sharedOpenViewModel.data.observe(viewLifecycleOwner) { newData ->
            Log.d("kkim", "데이터가 갱신됐따! ${newData}")
            // adapter.submitList(newData)
            openWeatherTimeAdapter.updateData(newData)
        }

    }

    override fun onLocationChanged(location: Location) {

        val address = getAddress(location.latitude, location.longitude)

        address?.takeIf { it.isNotEmpty() }?.get(0)?.let { firstAddress ->
            // locationTextView.text = "${firstAddress.adminArea} ${firstAddress.locality} ${firstAddress.thoroughfare}"
            locationTextView.text = "${firstAddress.adminArea} ${firstAddress.thoroughfare}"
        }
    }
}


//class NonScrollLinearLayoutManager(context: Context) : LinearLayoutManager(context) {
//
//    override fun canScrollHorizontally(): Boolean {
//        // 가로 스크롤을 막음
//        return false
//    }
//}