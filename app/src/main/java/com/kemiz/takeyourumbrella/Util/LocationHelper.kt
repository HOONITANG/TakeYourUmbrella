package com.kemiz.takeyourumbrella.Util


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.Priority

interface LocationCallbackInterface {
    fun onLocationChanged(location: android.location.Location)
}

class LocationHelper(private val context: Context, private val callback: LocationCallbackInterface) {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    init {
        initLocationClient()
        initLocationCallback()
    }

    private fun initLocationClient() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    }

    private fun initLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                p0?.lastLocation?.let { location ->
                    onLocationChanged(location)
                    removeLocationUpdates()
                } ?: run {
                    requestLocation()
                }
            }
        }
    }

    fun requestLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            requestLocation()
        } else {
            ActivityCompat.requestPermissions(
                context as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }

    private fun requestLocation() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            try {
                val locationRequest = createLocationRequest()
                fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
            } catch (se: SecurityException) {
                // 권한이 거부된 경우 사용자에게 설명이나 알림을 통해 다시 요청할 수 있도록 유도
            }
        } else {
            ActivityCompat.requestPermissions(
                context as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }

    private fun createLocationRequest(): LocationRequest {
        val timeInterval: Long = 10000  //위치 업데이트 간격 (밀리초)
        val minimalDistance = 0.0F

        return LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, timeInterval).apply {
            setMinUpdateDistanceMeters(minimalDistance)
            setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)    // 우선순위
            setWaitForAccurateLocation(true)
        }.build()
    }

    private fun removeLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }


//    private fun onLocationChanged(location: android.location.Location) {
//        // 위치 정보가 업데이트되면 호출
//        val latitude = location.latitude
//        val longitude = location.longitude
//        // 여기서 위치 정보를 사용하여 작업 수행
//    }

    private fun onLocationChanged(location: Location) {
        // 위치 정보가 업데이트되면 호출
        callback.onLocationChanged(location)
    }

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1001
    }
}