package com.kemiz.takeyourumbrella.Util
import android.app.Activity
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.kemiz.takeyourumbrella.MainActivity
import com.kemiz.takeyourumbrella.R
import java.lang.ref.WeakReference

object LoadingScreen {

    private var loadingView: FrameLayout? = null
    private var weakActivity: WeakReference<Activity>? = null

    fun show(activity: Activity) {
        weakActivity = WeakReference(activity)

        if (loadingView == null) {
            val inflater = LayoutInflater.from(activity)
            loadingView = inflater.inflate(R.layout.loading_screen, activity.findViewById(android.R.id.content), false) as FrameLayout?
        }

        loadingView?.let {
            it.isVisible = true
            weakActivity?.get()?.addContentView(it, it.layoutParams)
        }
    }

    fun dismiss() {
        loadingView?.isVisible = false
    }

    fun simulateLoadingAndDismiss(callback: () -> Unit) {
        weakActivity?.get()?.let {
            show(it)

            Handler().postDelayed({
                dismiss()
                callback.invoke()
            }, 3000) // Simulate a delay of 3000 milliseconds (adjust as needed)
        }
    }
}