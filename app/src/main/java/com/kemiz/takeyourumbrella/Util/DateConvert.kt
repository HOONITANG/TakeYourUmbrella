package com.kemiz.takeyourumbrella.Util
import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


class DateConvert {
    private val utcFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    private val kstFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    private val hourFormat = SimpleDateFormat("HH", Locale.getDefault())

    init {
        utcFormat.timeZone = TimeZone.getTimeZone("UTC")
        kstFormat.timeZone = TimeZone.getTimeZone("Asia/Seoul")
    }

//    fun getCurrentTime(): Pair<String,String> {
//
//
//
//        val mFormatTime = SimpleDateFormat("HH00")
//        // val formatTime: String = (mFormatTime.format(mReDate).toInt() - 100).toString()     // 1시간 이전 데이터를 요청함.
//        val formatTime: String = getCalculateBaseDateTime()               // 필터링에 걸리는 시간을 택함
//        return Pair(formatYDM, formatTime)
//    }

    fun getYesterday(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1) // 현재 날짜에서 1일을 빼면 어제의 날짜가 됨

        val dateFormat = SimpleDateFormat("yyyyMMdd")
        return dateFormat.format(calendar.time)
    }

    fun getLastUpdatedDate(inputDate: String): String {
        try {
            // Define input and output date formats
            val inputFormat = SimpleDateFormat("yyyyMMdd HHmm", Locale.getDefault())
            val outputFormat = SimpleDateFormat("MM월dd일 / a hh:mm", Locale.getDefault())

            // Parse the input date string
            val date = inputFormat.parse(inputDate)

            // Format the date and return the result
            return outputFormat.format(date)
        } catch (e: ParseException) {
            // Handle parsing errors
            e.printStackTrace()
            return "Invalid Date"
        }
    }


    fun getCalculateBaseDateTime(): Pair<String,String> {
        val mNow: Long = System.currentTimeMillis()
        val mReDate = Date(mNow)

        val mFormatYDM = SimpleDateFormat("yyyyMMdd")              // 현재 날짜 데이터 년월일
        var formatYDM: String = mFormatYDM.format(mReDate)

        val currentTime = SimpleDateFormat("HHmm").format(Date()).toInt()

        val apiTimes = listOf(210, 510, 810, 1110, 1410, 1710, 2010, 2310)

        var nearestApiTime = apiTimes.lastOrNull { it < currentTime}
            ?: apiTimes.last() // 만약 현재 시간이 모든 API 제공 시간보다 크다면 마지막 apiTime 값

        // 만약 현재 시간이 가장 마지막 API 제공 시간보다 작다면, 이전 날짜와 마지막 apiTime 값
        if (currentTime < apiTimes.first()) {
            nearestApiTime = apiTimes.last()
            formatYDM = getYesterday()
        }

        val formatTime = String.format("%04d", nearestApiTime)

        return Pair(formatYDM, formatTime)
    }

    fun convertUtcToKst(utcTime: String): String {
        return try {
            val utcDate = utcFormat.parse(utcTime)
            kstFormat.format(utcDate)
        } catch (e: Exception) {
            e.printStackTrace()
            utcTime
        }
    }

    fun convertKstToHour(kstTime: String): String {
        return try {
            val kstDate = kstFormat.parse(kstTime)
            hourFormat.format(kstDate)
        } catch (e: Exception) {
            e.printStackTrace()
            kstTime
        }
    }

    fun parseDate(dateString: String, format: String): Date? {
        val sdf = SimpleDateFormat(format, Locale.getDefault())
        return try {
            sdf.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun extractFormattedDateTimeFromDate(date: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        return dateFormat.format(date)
    }

    fun isNextDay(compareDateString: String, daysToAdd: Long): Boolean {
        val currentDate = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
        val compareDate = LocalDate.parse(compareDateString, formatter)

        // 현재 날짜와 비교 날짜가 1일 차이가 나면 true 반환
        return currentDate.plusDays(daysToAdd) == compareDate
    }

    // UTC -> 지역 시간으로 변경
    fun convertToUserLocalTime(utcTime: String): Date? {
        return try {
            val apiTime = utcFormat.parse(utcTime)

            // 사용자의 현재 지역 정보 얻기
            val userTimeZone = TimeZone.getDefault()

            // API에서 받은 UTC 시간을 사용자의 시간대로 변환
            val userLocalTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).apply {
                timeZone = userTimeZone
            }

            val userLocalTimeString = userLocalTimeFormat.format(apiTime)

            // 사용자의 시간대로 변환된 시간을 Date 형식으로 반환
            userLocalTimeFormat.parse(userLocalTimeString)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}

// 위치를 받아서 시간변환을 하는 작업!
//import android.content.Context
//import android.location.Geocoder
//import android.location.Location
//import android.location.LocationManager
//import java.time.LocalDateTime
//import java.time.ZoneId
//import java.time.format.DateTimeFormatter
//import java.util.*
//
//class DateConvert(private val context: Context) {
//    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
//
//    fun getCurrentLocation(): Location? {
//        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//
//        // 사용 가능한 위치 제공자(GPS 또는 네트워크)를 통해 현재 위치 획득
//        val providers: List<String> = locationManager.getProviders(true)
//        var bestLocation: Location? = null
//        for (provider in providers) {
//            val location: Location = locationManager.getLastKnownLocation(provider) ?: continue
//            if (bestLocation == null || location.accuracy < bestLocation.accuracy) {
//                bestLocation = location
//            }
//        }
//
//        return bestLocation
//    }
//
//    fun convertToCurrentTimeZone(originalTime: String): String {
//        val currentLocation = getCurrentLocation()
//        val currentZoneId = determineTimeZone(currentLocation)
//        val originalDateTime = LocalDateTime.parse(originalTime, formatter)
//
//        val convertedDateTime = originalDateTime.atZone(ZoneId.of(currentZoneId))
//        return convertedDateTime.format(formatter)
//    }
//
//    private fun determineTimeZone(location: Location?): String {
//        return if (location != null) {
//            val geocoder = Geocoder(context, Locale.getDefault())
//            val addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
//            if (addresses.isNotEmpty()) {
//                val timeZoneId = TimeZone.getTimeZone(addresses[0].timeZone.id)
//                timeZoneId.id
//            } else {
//                "UTC" // 기본적으로 UTC 사용
//            }
//        } else {
//            "UTC" // 기본적으로 UTC 사용
//        }
//    }
//}