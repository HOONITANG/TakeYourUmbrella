package com.kemiz.takeyourumbrella


import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.kemiz.takeyourumbrella.Data.APIMethod
import com.kemiz.takeyourumbrella.Data.DataCombiner
import com.kemiz.takeyourumbrella.Screen.HomeFragment
import com.kemiz.takeyourumbrella.Screen.SettingFragment
import com.kemiz.takeyourumbrella.Util.DateConvert
import com.kemiz.takeyourumbrella.Util.LoadingScreen
import com.kemiz.takeyourumbrella.Util.LocationCallbackInterface
import com.kemiz.takeyourumbrella.Util.LocationHelper
import com.kemiz.takeyourumbrella.Util.TO_GRID
import com.kemiz.takeyourumbrella.Util.TransLocalPoint
import com.kemiz.takeyourumbrella.Util.TransLocalPoint.LatXLngY
import com.kemiz.takeyourumbrella.ViewModel.GisangWeatherTimeViewModel
import com.kemiz.takeyourumbrella.ViewModel.OpenWeatherTimeViewModel
import com.kemiz.takeyourumbrella.ViewModel.TomorrowWeatherTimeViewModel
import com.kemiz.takeyourumbrella.ViewModel.WeatherTimeSectionViewModel
import com.kemiz.takeyourumbrella.databinding.ActivityMainBinding
import com.kemiz.takeyourumbrella.retrofit.ApiManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.http.Query


class MainActivity : AppCompatActivity(), LocationCallbackInterface {

    lateinit var binding: ActivityMainBinding

    private lateinit var sharedTimeSectionViewModel: WeatherTimeSectionViewModel
    private lateinit var sharedGisangViewModel: GisangWeatherTimeViewModel
    private lateinit var sharedTomorrowViewModel: TomorrowWeatherTimeViewModel
    private lateinit var sharedOpenViewModel: OpenWeatherTimeViewModel
    private lateinit var locationHelper: LocationHelper

    private lateinit var homeFragment: HomeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // 위치 정보 권한 요청 및 위치 정보 획득
        locationHelper = LocationHelper(this, this)
        locationHelper.requestLocationUpdates()

        homeFragment = HomeFragment()
        replaceFragment(homeFragment)

        sharedTimeSectionViewModel = ViewModelProvider(this).get(WeatherTimeSectionViewModel::class.java)
        sharedGisangViewModel = ViewModelProvider(this).get(GisangWeatherTimeViewModel::class.java)
        sharedTomorrowViewModel = ViewModelProvider(this).get(TomorrowWeatherTimeViewModel::class.java)
        sharedOpenViewModel = ViewModelProvider(this).get(OpenWeatherTimeViewModel::class.java)

        binding.bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.home -> replaceFragment(HomeFragment())
                // R.id.setting -> replaceFragment(SettingFragment())
                else -> { }
            }
            true
        }

    }

    override fun onLocationChanged(location: android.location.Location) {
        // 위치 정보가 업데이트되면 호출되는 메서드
        val latitude = location.latitude
        val longitude = location.longitude
        // 여기서 위치 정보를 사용하여 작업 수행

        Log.d("kkim","latitude is ${latitude}")
        Log.d("kkim","longitude is ${longitude}")

        weatherAPICall(latitude, longitude)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LocationHelper.LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // 위치 권한이 허용된 경우
                // 위치 정보 권한 요청 및 위치 정보 획득
                locationHelper = LocationHelper(this, this)
                locationHelper.requestLocationUpdates()
            } else {
                // 위치 권한이 거부된 경우
                // 사용자에게 설명이나 알림을 통해 다시 요청할 수 있도록 유도
            }
        }
    }

    private fun weatherAPICall(latitude: Double, longitude: Double) {
        /* 위치 권한 데이터 호출 */
        /* 위치 데이터 호출 */
        LoadingScreen.show(this)
        /* weather API 호출 */
        val apiManager = ApiManager.getInstance()
        val weatherApi = apiManager.createGisangWeatherApi()
        val tomorrowWeatherApi = apiManager.createTomorrowWeatherApi()

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val transLocalPoint = TransLocalPoint()
                // GPS 위치 변환 (기상청용)
                val convertGPS: LatXLngY? = transLocalPoint.convertGRID_GPS(TO_GRID, latitude, longitude)
                val nx = String.format("%.0f",convertGPS?.x)
                val ny = String.format("%.0f",convertGPS?.y)

                // 현재 시간 정보 (dateTime은 1시간 이전 시간) (기상청용)
                val ( dateYDM, dateTime ) = DateConvert().getCalculateBaseDateTime()

                val updatedTime = "업데이트: " + DateConvert().getLastUpdatedDate("${dateYDM} ${dateTime}")
                // last Updated: pm 8:00
                homeFragment.updateLastedTimeText(updatedTime)

                val gisangList = weatherApi.doGetWeatherList(nx = nx, ny = ny, base_date = dateYDM, base_time = dateTime)
                val titles = DataCombiner().createTitles(gisangList)
                val gisangData = DataCombiner().createIntegratedWeatherData(data = gisangList, type = APIMethod.GISANG)?.filter { it.title in titles }
                var tomorrowList = tomorrowWeatherApi.doGetWeatherList(location = "${latitude}, ${longitude}")
                val tomorrowData = DataCombiner().createIntegratedWeatherData(data = tomorrowList, type = APIMethod.TOMORROW)?.filter { it.title in titles }

                withContext(Dispatchers.Main) {
                    // UI 업데이트 등을 수행
                    sharedTimeSectionViewModel.updateData(titles)
                    sharedGisangViewModel.updateData(gisangData)
                    sharedTomorrowViewModel.updateData(tomorrowData)
                    LoadingScreen.dismiss()
                }
            } catch (e: Exception) {
                Log.d("kkim","e 값: ${e}")
                // Handle error
                withContext(Dispatchers.Main) {
                    // UI 업데이트 등을 수행
                    // Observer로 전달할거라 안해도 될 것 같기도함.
                }
            }
        }

    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout, fragment)
        fragmentTransaction.commit()
    }



//    suspend fun fetchDataConcurrently(): Pair<Call<WeatherResponse>, Call<TomorrowWeatherModel>> {
//        val apiManager = ApiManager.getInstance()
//        // 2) 호출하는 함수 콜 만들기.
//        // 인터페이스에 등록된 추상 함수를 이용함.
//        val weatherApi = apiManager.createGisangWeatherApi()
//        val tomorrowWeatherApi = apiManager.createTomorrowWeatherApi()
////        val gisangListCall = weatherApi.doGetWeatherList(nx = "55", ny = "127")
////        var tomorrowListCall = tomorrowWeatherApi.doGetWeatherList(location = "55, 127")
//
//        return coroutineScope {
//            // Use async to perform the API calls concurrently
//            val deferred1 = async { weatherApi.doGetWeatherList(nx = "55", ny = "127") }
//            val deferred2 = async { tomorrowWeatherApi.doGetWeatherList(location = "55, 127") }
//
//            // Await for both API calls to complete
//            val result1 = deferred1.await()
//            val result2 = deferred2.await()
//
//            // Return the results as a Pair
//            Pair(result1,result2)
//        }
//    }
}