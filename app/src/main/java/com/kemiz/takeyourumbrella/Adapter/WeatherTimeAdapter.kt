package com.kemiz.takeyourumbrella.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.kemiz.takeyourumbrella.Data.IntegratedWeatherModel
import com.kemiz.takeyourumbrella.Data.VerticalItem
import com.kemiz.takeyourumbrella.R
import java.text.SimpleDateFormat
import java.util.*

class WeatherTimeAdapter(private var dataList: List<IntegratedWeatherModel>) :
    RecyclerView.Adapter<WeatherTimeAdapter.WeatherViewHolder>() {

    class WeatherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewTimeVertical: ImageView = itemView.findViewById(R.id.imageViewTimeVertical)
        val textViewTimeDegrees: TextView = itemView.findViewById(R.id.timeDegreesTextView)
        val textViewTimePrecipitationForm: TextView = itemView.findViewById(R.id.timePrecipitationForm)
        val textViewTimePercentage: TextView = itemView.findViewById(R.id.timePercentageTextView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.time_horizontal_row_vertical_item, parent, false)

        return WeatherViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val item = dataList[position]
        // holder.imageViewTimeVertical.setImageResource(item.)
        holder.textViewTimePrecipitationForm.visibility = View.VISIBLE
        holder.textViewTimePrecipitationForm.text = item.precipitationForm
        holder.imageViewTimeVertical.visibility = View.GONE
        holder.textViewTimeDegrees.text = item.temperature + "℃"
        holder.textViewTimePercentage.text = item.precipitation + "%"
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateData(newData: List<IntegratedWeatherModel>) {
        dataList = newData
        notifyDataSetChanged()
    }
}