package com.kemiz.takeyourumbrella.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kemiz.takeyourumbrella.Data.IntegratedWeatherModel
import com.kemiz.takeyourumbrella.Data.TimeHorizontalViewSectionHeader
import com.kemiz.takeyourumbrella.Data.VerticalItem
import com.kemiz.takeyourumbrella.R
import kotlinx.coroutines.NonDisposableHandle.parent
import java.text.SimpleDateFormat
import java.util.*

class WeatherTimeSectionAdapter(private var dataList: List<TimeHorizontalViewSectionHeader>) :
    RecyclerView.Adapter<WeatherTimeSectionAdapter.SectionHeaderViewHolder>() {

    class SectionHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewSection: TextView = itemView.findViewById(R.id.textViewSection)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionHeaderViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.time_horizontal_row_vertical_section, parent, false)

        return SectionHeaderViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SectionHeaderViewHolder, position: Int) {
        val item = dataList[position]
        holder.textViewSection.text = item.title
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateData(newData: List<TimeHorizontalViewSectionHeader>) {
        dataList = newData
        notifyDataSetChanged()
        Log.d("kkim","데이터 변경까지 오케이 $newData")
    }
}