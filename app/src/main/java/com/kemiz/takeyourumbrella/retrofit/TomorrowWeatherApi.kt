package com.kemiz.takeyourumbrella.retrofit

import com.kemiz.takeyourumbrella.Data.TomorrowWeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TomorrowWeatherApi {
    @GET("forecast")
    suspend fun doGetWeatherList(
        @Query("apikey", encoded = true) serviceKey: String = "6kuzNYI5V9tJRUGE6twvtQRZMUdgZmy1",
        @Query("timesteps") timesteps: String = "hourly",
        @Query("location") location: String = "42.3478, -71.0466"
    ): TomorrowWeatherResponse
}