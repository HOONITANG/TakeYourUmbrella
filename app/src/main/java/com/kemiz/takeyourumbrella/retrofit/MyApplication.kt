package com.kemiz.takeyourumbrella.retrofit

import android.app.Application
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// 자주 사용이 될 네트워크 인터페이스를, 미리 시스템에 등록
// 메니페스트에 등록해서,
// 앱이 실행되면, 해당 MyApplication 기능이
// 메모리에 등록이 되어서 사용하기 편함.
//class UrlLoggingInterceptor : Interceptor {
//    override fun intercept(chain: Interceptor.Chain): Response {
//        val request: Request = chain.request()
//        val url = request.url.toString()
//
//        // Log the URL before the request is executed
//        println("Request URL: $url")
//
//        // Proceed with the request
//        return chain.proceed(request)
//    }
//}
//
//class MyApplication: Application() {
//
//    // 1) 통신에 필요한 인스턴스를 선언 및 초기화
//    val networkService: INetworkService
//
//    val loggingInterceptor = HttpLoggingInterceptor().apply {
//        level = HttpLoggingInterceptor.Level.BODY
//    }
//
//    val urlLoggingInterceptor = UrlLoggingInterceptor()
//
//    val responseLoggingInterceptor = Interceptor { chain ->
//        val request = chain.request()
//        val response = chain.proceed(request)
//        val responseBody = response.body
//        val responseString = responseBody?.string()
//
//        // Log the response JSON
//        println("Response JSON: $responseString")
//
//        // Rebuild the response body to avoid consuming it
//        val newResponseBody = responseString?.toResponseBody(responseBody?.contentType())
//        response.newBuilder().body(newResponseBody).build()
//    }
//
//    val client = OkHttpClient.Builder()
//        .addInterceptor(loggingInterceptor)
//        .addInterceptor(urlLoggingInterceptor)
//        .build()
//
//    // 2) 통신할 서버의 URL 주소를 등록함.
//    val retrofit: Retrofit
//        get() = Retrofit.Builder()
//            .baseUrl("https://apis.data.go.kr/")
//            .addConverterFactory(GsonConverterFactory.create())
//            .client(client)
//            .build()
//    // 초기화 할당하는 부분.
//    init {
//        networkService = retrofit.create(INetworkService::class.java)
//    }
//}