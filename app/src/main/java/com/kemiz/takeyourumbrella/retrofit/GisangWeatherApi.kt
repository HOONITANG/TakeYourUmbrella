package com.kemiz.takeyourumbrella.retrofit


import com.kemiz.takeyourumbrella.Data.GisangWeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GisangWeatherApi {
    // 기상청 날씨
    @GET("1360000/VilageFcstInfoService_2.0/getVilageFcst")
    // baseurl: https://reqres.in/
    // get: 가져올 주소 api/users?page=2
    // 함수호출 예시 -> doGetUserList("3")
    // 반환타입 Call, <>에 담겨진 타입 내용을 전달받음
    suspend fun doGetWeatherList(
        @Query("serviceKey", encoded = true) serviceKey: String = "fp9ZFJ07Vvb4djBjCFjoq67eXYDIe23cZpN8vtOGcGI0nRz5TdSKkWDTMdtfzryzz%2BzdiqDAQ1RmkGRu4EJURQ%3D%3D",
        @Query("pageNo") pageNo: String = "1",
        @Query("numOfRows") numOfRows: String = "1000",
        @Query("dataType") dataType: String = "JSON",
        @Query("base_date") base_date: String = "20240102",
        @Query("base_time") base_time: String = "0800",
        @Query("nx", encoded = true) nx: String,
        @Query("ny", encoded = true) ny: String
    ): GisangWeatherResponse
}