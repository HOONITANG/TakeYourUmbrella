package com.kemiz.takeyourumbrella.retrofit

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManager {
    private var weatherRetrofit: Retrofit
    private var tomorrowWeatherRetrofit: Retrofit
    val gson = GsonBuilder().setLenient().create()
    init {
        weatherRetrofit = createRetrofit("https://apis.data.go.kr/")
        tomorrowWeatherRetrofit = createRetrofit("https://api.tomorrow.io/v4/weather/")

    }

    companion object {
        private var instance: ApiManager? = null

        fun getInstance(): ApiManager {
            Log.d("kkim","왜죽는건가? ${instance}")
            if (instance == null) {
                instance = ApiManager()
            }

            return instance!!
        }
    }

    fun createGisangWeatherApi(): GisangWeatherApi {
        return createApiService(weatherRetrofit)
    }

    fun createTomorrowWeatherApi(): TomorrowWeatherApi {
        return createApiService(tomorrowWeatherRetrofit)
    }

    private fun createRetrofit(baseUrl: String): Retrofit {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        val responseLoggingInterceptor = Interceptor { chain ->
            val request = chain.request()
            val response = chain.proceed(request)
            val responseBody = response.body
            val responseString = responseBody?.string()

            // Log the response JSON
            println("Response JSON: $responseString")

            // Rebuild the response body to avoid consuming it
            val newResponseBody = responseString?.toResponseBody(responseBody?.contentType())
            response.newBuilder().body(newResponseBody).build()
        }
        val client = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(responseLoggingInterceptor)
            .build()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            // .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            // Add any other configuration as needed
            .client(client)
            .build()
    }

    private inline fun <reified T> createApiService(retrofit: Retrofit): T {
        return retrofit.create(T::class.java)
    }
}