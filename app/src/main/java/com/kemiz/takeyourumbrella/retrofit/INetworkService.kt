//package com.kemiz.takeyourumbrella.retrofit
//
//import com.kemiz.takeyourumbrella.Data.WeatherResponse
//import okhttp3.ResponseBody
//import retrofit2.Call
//import retrofit2.http.GET
//import retrofit2.http.POST
//import retrofit2.http.Query
//import retrofit2.http.Url
//
//// 통신라이브러리 : retrofit2 이용해서
//// 인터페이스, 추상메서드를 만들어서,
//// 레트로핏한테 전달: 인터페이스 통으로 전달하면,
//// 여기에 정의된 함수를 이용해서 통신을 함. 기본적인 CRUD
//interface INetworkService {
////    @GET("api/users")
////    // baseurl: https://reqres.in/
////    // get: 가져올 주소 api/users?page=2
////    // 함수호출 예시 -> doGetUserList("3")
////    // 반환타입 Call, <>에 담겨진 타입 내용을 전달받음
////    fun doGetUserList(@Query("page") page: String): Call<GisangListModel>
////
////    // 프로필 이미지
////    @GET
////    fun getAvatarImage(@Url url:String): Call<ResponseBody>
//
//    // 기상청 날씨
//    @GET("1360000/VilageFcstInfoService_2.0/getUltraSrtNcst")
//    // baseurl: https://reqres.in/
//    // get: 가져올 주소 api/users?page=2
//    // 함수호출 예시 -> doGetUserList("3")
//    // 반환타입 Call, <>에 담겨진 타입 내용을 전달받음
//    fun doGetWeatherList(
//        @Query("serviceKey") serviceKey: String = "fp9ZFJ07Vvb4djBjCFjoq67eXYDIe23cZpN8vtOGcGI0nRz5TdSKkWDTMdtfzryzz%2BzdiqDAQ1RmkGRu4EJURQ%3D%3D",
//        @Query("pageNo") pageNo: String = "1",
//        @Query("numOfRows") numOfRows: String = "1000",
//        @Query("dataType") dataType: String = "JSON",
//        @Query("base_date") base_date: String = "20231221",
//        @Query("base_time") base_time: String = "0600",
//        @Query("nx") nx: String,
//        @Query("ny") ny: String
//    ): Call<WeatherResponse>
//}