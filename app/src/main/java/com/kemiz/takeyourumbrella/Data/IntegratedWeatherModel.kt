package com.kemiz.takeyourumbrella.Data
import com.kemiz.takeyourumbrella.Util.DateConvert
import java.util.Date

//data class IntegratedWeatherModel(
//    val item: MutableMap<String, List<IntegratedWeatherModel>>  // 202312260500
//)

data class IntegratedWeatherModel(
    val title: Date,
    val temperature: String,                // 온도
    val precipitation: String,              // 강수확률
    val precipitationForm: String,          // 강수확률
    val apiMethod: APIMethod
)

enum class APIMethod {
    GISANG,
    TOMORROW,
    OPENWEATHERMAP;
}

class DataCombiner {
    // 날짜를 뽑고 // 리스트 호출할때 해당 날짜에 맞춰서 반환.
    //    titles: List<String> 현재 시간으로 타임 title 정함
    fun createTitles(gisang: GisangWeatherResponse?): List<Date> {
        val list = gisang?.response?.body?.items?.item

        val date = list
            ?.filter { it.category == "POP" }
            ?.map {
            val fcstDateTimeString: String = it.fcstDate + it.fcstTime
            val fcstDateTime = DateConvert().parseDate(fcstDateTimeString, "yyyyMMddHHmm") ?: Date()
            fcstDateTime
        }
        return date ?: emptyList()
    }

    fun createIntegratedWeatherData( data: Any, type: APIMethod): List<IntegratedWeatherModel>? {
        return when (type) {
            APIMethod.GISANG -> convertGisangData(data as? GisangWeatherResponse)
            APIMethod.TOMORROW -> convertTomorrowData(data as? TomorrowWeatherResponse)
            APIMethod.OPENWEATHERMAP -> convertGisangData(data as? GisangWeatherResponse)
        }
    }

    fun groupWeatherDataByDateAndTime(dataList: List<WeatherItem>): Map<Pair<String, String>, List<WeatherItem>> {
        return dataList.groupBy { it.fcstDate to it.fcstTime }
    }

    fun getSpecificData(groupedData: Map<Pair<String, String>, List<WeatherItem>>, baseDate: String, fcstTime: String): List<WeatherItem> {
        return groupedData[baseDate to fcstTime] ?: emptyList()
    }

    fun convertGisangData(data: GisangWeatherResponse?): List<IntegratedWeatherModel>? {
        val list = data?.response?.body?.items?.item

        val filterData = list
            ?.filter { it.category == "POP" || it.category == "PTY" || it.category == "TMP" } ?: listOf()

        val groupedData = groupWeatherDataByDateAndTime(filterData)


        val result = list
            ?.filter { it.category == "POP" }
            ?.map {
                // val specificData = getSpecificData(groupedData, "20240103", "2100")
                val groupedDataByCategoryPOPPTY = getSpecificData(groupedData, it.fcstDate, it.fcstTime)
                val fcstDateTimeString: String = it.fcstDate + it.fcstTime
                val dateTime = DateConvert().parseDate(fcstDateTimeString, "yyyyMMddHHmm") ?: Date()

                val precipitation = groupedDataByCategoryPOPPTY.first { gd -> gd.category == "POP" }.fcstValue
                val precipitationForm = groupedDataByCategoryPOPPTY.first { gd -> gd.category == "PTY" }.fcstValue
                val temperature = groupedDataByCategoryPOPPTY.first { gd -> gd.category == "TMP" }.fcstValue

                IntegratedWeatherModel(
                    title = dateTime,
                    temperature = temperature,
                    precipitation = precipitation,
                    precipitationForm = GisangPrecipitationForm.fromValue(precipitationForm.toInt()).toString(),
                    apiMethod = APIMethod.GISANG)

            }

//        val result = list
//            ?.filter { it.category == "POP" || it.category == "PTY" }
//            ?.map {
//
//                val index = list.indexOf(it)
//
//                val fcstDateTimeString: String = it.fcstDate + it.fcstTime
//                val dateTime = DateConvert().parseDate(fcstDateTimeString, "yyyyMMddHHmm") ?: Date()
//                // val precipitationForm = precipitationFormList?.get(index) ?: PrecipitationForm.NONE
//                IntegratedWeatherModel(title = dateTime, temperature = "", precipitation = "${it.fcstValue}", precipitationForm = PrecipitationForm.NONE, apiMethod = APIMethod.GISANG)
//        }

        return result
    }

    fun convertTomorrowData(data: TomorrowWeatherResponse?): List<IntegratedWeatherModel>? {
        val list = data?.timelines?.hourly
        val result = list?.map {
            val utcTime = it.time
            // val kstTime = DateConvert().convertUtcToKst(utcTime
            val dateTime = DateConvert().convertToUserLocalTime(utcTime) ?: Date()
            val temperature = it.values.temperature
            val precipitation = it.values.precipitationProbability
            val precipitationForm = it.values.weatherCode

            IntegratedWeatherModel(
                title = dateTime,
                temperature="$temperature",
                precipitation =" $precipitation",
                precipitationForm =  TomorrowWeatherType.fromValue(precipitationForm.toString()).toString(),
                apiMethod = APIMethod.TOMORROW)
        }
        return result
    }



}


//fun integrateModels(type: APIMethod, gisang: GisangWeatherResponse?, tomorrow: TomorrowWeatherResponse?): List<IntegratedWeatherModel> {
//    val groupedModels = mutableListOf<IntegratedWeatherModel>()
//    val titles = createTitles(gisang)
//    titles.forEach { title ->
//
//    }
//
//    // 초기화
//    for ((index, model) in titles.withIndex()) {
//        val dateKey = model.toString()
//        groupedModels[index].item[dateKey] = listOf()
//    }
//
//    val list1 = gisang?.response?.body?.items?.item
//    // Combine models from list1
//    list1?.forEach { model ->
//        val fcstDateTimeString: String = model.fcstDate + model.fcstTime
//        val list1Date= DateConvert().parseDate(fcstDateTimeString, "yyyyMMddHHmm") ?: Date()
//        val list1DateKey = list1Date.toString()
//
//
//        // 값을 찾아 해당 위치, 데이터를 업데이트함.
//        val foundItem = groupedModels.find { it.item.containsKey(list1DateKey) }
//        if (foundItem != null) {
//            val valueForKey = foundItem.item[list1DateKey]
//            println("Item found with key '$list1DateKey': $valueForKey")
//        } else {
//            println("Item not found with key '$list1DateKey'")
//        }
//
//    }
//
//    return groupedModels.values.toList()
//}
//class DataCombiner {
//    // titles: List<String>: 현재 시간으로 타임 title 정함.
//    // list1: List<WeatherModel1>?: 기상청 List 데이터. 우선 api데이터 추출 후 리스트 찾기..
//    // list2: List<WeatherModel2>?
//    // 데이터를 IntegreModel에 맞게 변환 후, Title(Time)기준으로 하나로 합쳐주자!
//    fun integrateModels(titles: List<String>, gisang: GisangWeatherResponse?, tomorrow: TomorrowWeatherResponse?): List<IntegratedWeatherModel> {
//        val groupedModels = mutableMapOf<String, IntegratedWeatherModel>()
//
//        val list1 = gisang?.response?.body?.items?.item
//        val list2_value_list = tomorrow?.timelines?.hourly?.map { it.values }
//        val list2_title = tomorrow?.timelines?.hourly?.map { it.time }
//
//
//        // Initialize IntegratedWeatherModel for each title
//        titles.forEach { title ->
//            groupedModels[title] = IntegratedWeatherModel(
//                title = title,
//                temperature = "",
//                precipitation = "",
//                apiMethod = APIMethod.UNKNOWN
//            )
//        }
//
//        // Combine models from list1
//        list1?.forEach { model ->
//            val integratedModel = groupedModels[model.title]
//            if (integratedModel != null) {
//                integratedModel.temperature = model.temperature
//            }
//        }
//
//        // Combine models from list2
//        list2?.forEach { model ->
//            val integratedModel = groupedModels[model.title]
//            if (integratedModel != null) {
//                integratedModel.precipitation = model.precipitation
//            }
//        }
//
//        // Combine models from list3
//        list3?.forEach { model ->
//            val integratedModel = groupedModels[model.title]
//            if (integratedModel != null) {
//                integratedModel.apiMethod = model.apiMethod
//            }
//        }
//
//        // Convert map values to list
//        return groupedModels.values.toList()
//    }
//}