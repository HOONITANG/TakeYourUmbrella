package com.kemiz.takeyourumbrella.Data

import com.google.gson.annotations.SerializedName
import okhttp3.ResponseBody
data class GisangWeatherResponse(
    val response: WeatherResponseData
)

data class WeatherResponseData(
    val header: Header,
    val body: WeatherResponseBody
)

data class Header(
    val resultCode: String,
    val resultMsg: String
)

data class WeatherResponseBody(
    val dataType: String,
    val items: WeatherItems,
    val pageNo: Int,
    val numOfRows: Int,
    val totalCount: Int
)

data class WeatherItems(
    val item: List<WeatherItem>
)

data class WeatherItem(
    val baseDate: String,
    val baseTime: String,
    val category: String,
    val nx: Int,
    val ny: Int,
    val obsrValue: String,
    val fcstValue: String,
    val fcstDate: String,
    val fcstTime: String
)
enum class GisangPrecipitationForm {
    NONE,     // 없음 0
    RAIN,     // 비 1
    RAINSNOW, // 눈 비 2
    SNOW,     // 눈 3
    SHOWER;    // 소나기 4

    override fun toString(): String {
        return when (this) {
            NONE -> "없읍"
            RAIN -> "비"
            RAINSNOW -> "눈 비"
            SNOW -> "눈"
            SHOWER -> "소나기"
        }
    }
    companion object {
        fun fromValue(value: Int): GisangPrecipitationForm {
            return when (value) {
                0 -> NONE
                1 -> RAIN
                2 -> RAINSNOW
                3 -> SNOW
                4 -> SHOWER
                else -> NONE
            }
        }

    }
}