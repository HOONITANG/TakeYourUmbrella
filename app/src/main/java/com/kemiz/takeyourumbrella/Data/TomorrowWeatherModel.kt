package com.kemiz.takeyourumbrella.Data

data class TomorrowWeatherResponse(
    val timelines: Timelines,
    val location: Location
)

data class Timelines(
    val hourly: List<HourlyTimeline>
)

data class HourlyTimeline(
    val time: String,
    val values: WeatherValues
)

data class WeatherValues(
    val cloudBase: Double?,
    val cloudCeiling: Double?,
    val cloudCover: Double,
    val dewPoint: Double,
    val evapotranspiration: Double,
    val freezingRainIntensity: Double,
    val humidity: Double,
    val iceAccumulation: Double,
    val iceAccumulationLwe: Double,
    val precipitationProbability: Double,
    val pressureSurfaceLevel: Double,
    val rainAccumulation: Double,
    val rainAccumulationLwe: Double,
    val rainIntensity: Double,
    val sleetAccumulation: Double,
    val sleetAccumulationLwe: Double,
    val sleetIntensity: Double,
    val snowAccumulation: Double,
    val snowAccumulationLwe: Double,
    val snowDepth: Double,
    val snowIntensity: Double,
    val temperature: Double,
    val temperatureApparent: Double,
    val uvHealthConcern: Double,
    val uvIndex: Int,
    val visibility: Double,
    val weatherCode: Int,
    val windDirection: Double,
    val windGust: Double,
    val windSpeed: Double
)

data class Location(
    val lat: Double,
    val lon: Double,
    val name: String,
    val type: String
)

enum class TomorrowWeatherType(val code: String) {
    UNKNOWN("0"),
    CLEAR_SUNNY("1000"),
    MOSTLY_CLEAR("1100"),
    PARTLY_CLOUDY("1101"),
    MOSTLY_CLOUDY("1102"),
    CLOUDY("1001"),
    FOG("2000"),
    LIGHT_FOG("2100"),
    DRIZZLE("4000"),
    RAIN("4001"),
    LIGHT_RAIN("4200"),
    HEAVY_RAIN("4201"),
    SNOW("5000"),
    FLURRIES("5001"),
    LIGHT_SNOW("5100"),
    HEAVY_SNOW("5101"),
    FREEZING_DRIZZLE("6000"),
    FREEZING_RAIN("6001"),
    LIGHT_FREEZING_RAIN("6200"),
    HEAVY_FREEZING_RAIN("6201"),
    ICE_PELLETS("7000"),
    LIGHT_ICE_PELLETS("7102"),
    HEAVY_ICE_PELLETS("7101"),
    THUNDERSTORM("8000");

    companion object {
        fun fromValue(code: String): TomorrowWeatherType {
            return values().firstOrNull { it.code == code } ?: UNKNOWN
        }
    }

    override fun toString(): String {
        // 이 부분에서 원하는 형식으로 매핑
        return when (this) {
            UNKNOWN -> "알 수 없음"
            CLEAR_SUNNY -> "맑음"
            MOSTLY_CLEAR -> "대체로 맑음"
            PARTLY_CLOUDY -> "구름 조금"
            MOSTLY_CLOUDY -> "대체로 흐림"
            CLOUDY -> "흐림"
            FOG -> "안개"
            LIGHT_FOG -> "가벼운 안개"
            DRIZZLE -> "이슬비"
            RAIN -> "비"
            LIGHT_RAIN -> "가벼운 비"
            HEAVY_RAIN -> "폭우"
            SNOW -> "눈"
            FLURRIES -> "소나기 눈"
            LIGHT_SNOW -> "가벼운 눈"
            HEAVY_SNOW -> "폭설"
            FREEZING_DRIZZLE -> "이슬비"
            FREEZING_RAIN -> "동결 비"
            LIGHT_FREEZING_RAIN -> "가벼운 비"
            HEAVY_FREEZING_RAIN -> "폭우 비"
            ICE_PELLETS -> "우박"
            LIGHT_ICE_PELLETS -> "가벼운 우박"
            HEAVY_ICE_PELLETS -> "폭우 우박"
            THUNDERSTORM -> "천둥번개"
        }
    }
}